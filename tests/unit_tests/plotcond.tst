// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- ENGLISH IMPOSED -->

// The values of atan and its condition are regular on a smaller range:
// use a regular scale.
h=scf();
condnb_plotcond ( condnb_atancond , linspace(-1,1,1000) );
delete(h)
h=scf();
condnb_plotcond ( condnb_atancond , linspace(-1,1,1000)' );
delete(h)

// The condition values of acos are positive and of very different magnitudes :
// automatically switches to log scale.
h=scf();
condnb_plotcond ( condnb_acoscond , linspace(-1,1,1000) );
delete(h)
h=scf();
condnb_plotcond ( condnb_sincond , linspace(-4,4,1000) );
delete(h)

// The exp values are positive and of very different magnitudes :
// automatically switches to log scale.
h=scf();
condnb_plotcond ( condnb_expcond , linspace(-7e2,7.e2,1000) );
delete(h)
//
// A case where we plot a customized condition number function.
stacksize("max")
function x=myinvnorstd(p)
  mu = zeros(p)
  std = ones(p)
  q = 1-p
  x = cdfnor("X",mu,std,p,q)
endfunction
function [c,x]=myinvnorstdcond(p)
  for k = 1 : size(p,"*")
    [ck,xk] = condnb_condnum(myinvnorstd,p(k))
    c(k) = ck
    x(k) = xk
  end
endfunction
h=scf();
condnb_plotcond ( myinvnorstdcond , linspace(1e-5,1-1e-5,100) );
h.children(1).log_flags="nln";
delete(h)
//
// A case where we plot a customized condition number function, 
// which needs additionnal arguments.
function x=myinvnor(p,mu,std)
  q = 1-p
  x = cdfnor("X",ones(p)*mu,ones(p)*std,p,q)
endfunction
function [c,x]=myinvnorcond(p,mu,std)
  for k = 1 : size(p,"*")
    [ck,xk] = condnb_condnum(list(myinvnor,mu,std),p(k))
    c(k) = ck
    x(k) = xk
  end
endfunction
h=scf();
condnb_plotcond ( list(myinvnorcond,1,2) , linspace(1e-5,1-1e-5,100) );
h.children(1).log_flags="nln";
delete(h)

