// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

x = [-0.80 0.09 -0.44 -0.62 0.09 0.99 0.92]';
expected=43.888889;
[c, y] = condnb_prodcond(x);
assert_checkalmostequal(y,-1.61006124e-03);
assert_checkalmostequal(c,43.888889);

// Compare with approximate Jacobian
[cc,yc] = condnb_condnum ( prod , x ),
assert_checkalmostequal(yc,y);
assert_checkalmostequal(cc,cc);
