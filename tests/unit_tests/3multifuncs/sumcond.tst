// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


// A badly conditionned sum
xl = 10^(1:15);
x = [-xl xl+0.1]';
[c,y] = condnb_sumcond ( x );
assert_checkalmostequal ( y , 1.5 , 1 );
// We can get two answers  : c = 1.3675213675D+15 and c = 1.26984126984126950e+015,
// depending on the session.
assert_checkequal ( or(c==[1.367521367521367000D+15 1.26984126984126950e+015] ) , %t );

// A badly conditionned sum, with accurate result !
// The bad conditionning does not imply that the result is not accurate.
xl = 10^(1:15);
x = [-xl xl]';
[c,y] = condnb_sumcond ( x );
// We can get two answers : sum(x)=0 and sum(x) = 1.75 !!!
// It depends on the Scilab session: guess that this is an effect of the
// MKL.
assert_checkequal ( y , 0 );
assert_checkequal ( c , %inf );

