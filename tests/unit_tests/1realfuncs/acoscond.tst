// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

[c,y] = condnb_acoscond ( 0.5 );
assert_checkalmostequal ( y , 1.04719755119660 , 10*%eps );
assert_checkalmostequal ( c , 0.551328895421792 , %eps );
//
[c,y] = condnb_acoscond ( 0 );
assert_checkalmostequal ( y , %pi/2 ,%eps );
assert_checkequal ( c , 0 );
//
[c,y] = condnb_acoscond ( 1 );
assert_checkequal ( y , 0 );
assert_checkequal ( c , %inf );
//
[c,y] = condnb_acoscond ( -1 );
assert_checkequal ( y , %pi );
assert_checkequal ( c , %inf );
//

