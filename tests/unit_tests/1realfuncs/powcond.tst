// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

[c,y] = condnb_powcond ( 2 , 5 );
assert_checkequal ( c , 5 );
assert_checkequal ( y , 32 );

// Extremelly bad condition
[c,y] = condnb_powcond ( 1.000000000000001 , 1.e15 );
assert_checkalmostequal ( y , 2.718281828459043876 , 1 );
assert_checkalmostequal ( c , 1.e15 , %eps );

// Another one : http://bugzilla.scilab.org/show_bug.cgi?id=4048
[c,y] = condnb_powcond ( 0.9999999999999999 , -18014398509482000 );
assert_checkalmostequal ( y , 6.05836432903779269 , 1 );
assert_checkalmostequal ( c , 1.8D+16 , 0.1 );


