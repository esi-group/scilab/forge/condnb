// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


[c,y] = condnb_sqrtcond ( 2 );
assert_checkalmostequal ( y , 1.414213562373095145 , 10*%eps );
assert_checkequal ( c , 0.5 );

