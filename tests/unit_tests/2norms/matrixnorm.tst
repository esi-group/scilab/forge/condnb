// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

A = [1 1];
//
y=condnb_matrixnorm(A);
assert_checkequal ( y , 1);
//
y=condnb_matrixnorm(A,1);
assert_checkequal ( y , 1);
//
y=condnb_matrixnorm(A,%inf);
assert_checkequal ( y , 2);
//
y=condnb_matrixnorm(A,2);
assert_checkalmostequal ( y , sqrt(2), 10*%eps);
