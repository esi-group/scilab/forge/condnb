// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y] = condnb_tancond(x)
    // Computes the condition number of the tan function.
    //
    // Calling Sequence
    //   [c, y] = condnb_tancond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //
    // Description
    // Computes the condition number of the tan function.
    //
    // The condition number is:
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = \left| \frac{x}{\cos(x)\sin(x)} \right|
    //\end{eqnarray}
    //</latex>
    //
    // The tan function has a large condition number if
    //   <itemizedlist>
    //   <listitem>x is large,</listitem>
    //   <listitem>x = k*pi/2, where k is a nonzero integer.</listitem>
    //   </itemizedlist>
    //
    // Examples
    //  [c, y] = condnb_tancond(1);
    //  [c, y] = condnb_tancond(0); // 1
    //  [c, y] = condnb_tancond(%pi/2);
    //  [c, y] = condnb_tancond(%pi);
    //
    //  condnb_plotcond(condnb_tancond, linspace(-4, 4, 1000));
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_tancond", rhs, 1:1);
    apifun_checklhs("condnb_tancond", lhs, 0:2);

    // Check type
    apifun_checktype("condnb_tancond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    y = tan(x);
    c = zeros(x);
    k = find(x <> 0);
    c(k) =  abs(x(k)) ..
         ./ abs((cos(x(k)).^2) ..
         .* y(k));
    k = find(x == 0);
    c(k) = 1;
endfunction

