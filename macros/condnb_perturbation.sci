// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function rp=condnb_perturbation(varargin)
    // Computes the (relative) perturbation
    //
    // Calling Sequence
    //   rp = condnb_perturbation(condfunc, x, delta)
    //   rp = condnb_perturbation(condfunc, x, delta, normflag)
    //
    // Parameters
    //   condfunc : a function, the condition number function
    //   x : a nx-by-1 matrix of doubles, nonzero, the current point
    //   delta : a nx-by-p matrix of doubles, the increments, column-by-column
    //   normflag : a double, the norm (default=1). The available values are normflag=1 for the 1-norm, normflag=2 for the Euclidian norm and normflag=%inf for the infinite-norm.
    //   rp : a 1-by-p matrix of doubles, the relative perturbation
    //
    // Description
    //   Computes the relative perturbation of f. 
    //
    // We assume that x and y=f(x) are nonzero.
    //
    // The relative error between <latex>x</latex> 
    // and <latex>\tilde{x}</latex> is :
    //
    //<latex>
    //\begin{eqnarray}
    //RE(x,\tilde{x}) = \frac{\|x-\tilde{x}\|}{\|x\|}
    //\end{eqnarray}
    //</latex>
    //
    // The relative perturbation of the function f to an increment 
    // in the input is :
    //
    //<latex>
    //\begin{eqnarray}
    //RP(x,\Delta) = \frac{RE(y,\tilde{y})}{RE(x,\tilde{x})}
    //\end{eqnarray}
    //</latex>
    //
    // where
    //
    //<latex>
    //\begin{eqnarray}
    //\tilde{x}=x+\Delta, \quad y=f(x), \quad \tilde{y}=f(\tilde{x}).
    //\end{eqnarray}
    //</latex>
    //
    // Examples
    // TODO
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_perturbation", rhs, 3:4);
    apifun_checklhs("condnb_perturbation", lhs, 0:1);

    // Get arguments
    condfunc = varargin(1);
    x = varargin(2);
    delta = varargin(3);
    normflag = apifun_argindefault(varargin, 4, 1);

    // Check types
    apifun_checktype("condnb_perturbation", condfunc, "condfunc", 1, ["function","list"]);
    apifun_checktype("condnb_perturbation", x, "x", 2, "constant");
    apifun_checktype("condnb_perturbation", delta, "delta", 3, "constant");
    apifun_checktype("condnb_perturbation", normflag, "normflag", 4, "constant");

    // Check size
    // condfunc : nothing to check
    nx=size(x,"r")
    apifun_checkdims("condnb_perturbation", x, "x", 2, [nx,1]);
    p=size(delta,"c")
    apifun_checkdims("condnb_perturbation", delta, "delta", 3, [nx,p]);
    apifun_checkscalar("condnb_perturbation", normflag, "normflag", 4);

    // Check content
    apifun_checkoption("condnb_perturbation", normflag, "normflag", 4, [1 2 %inf]);

    // Temporary enable IEEE mode, in order to generate 
    // NANs and INFs when appropriate.
    mod=ieee()
    ieee(2)
    [c,y,jac]=condfunc(x)
    p=size(delta,"c")
    rp=zeros(1,p)
    for i=1:p
        xtilde=x+delta(:,i)
        [c,y,jac]=condfunc(x)
        [c,ytilde,jac]=condfunc(xtilde)
        yperturbation=abs(y-ytilde)/abs(y)
        xperturbation=abs(norm(x,normflag)-norm(xtilde,normflag))/norm(x,normflag)
        rp(i)=yperturbation/xperturbation
    end
    ieee(mod)

endfunction
