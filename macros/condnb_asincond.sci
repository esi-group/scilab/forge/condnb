// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y] = condnb_asincond(x)
    // Computes the condition number of the asin function.
    //
    // Calling Sequence
    //   [c, y] = condnb_asincond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //
    // Description
    // Computes the condition number of the asin function.
    //
    // The condition number is :
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = \left| \frac{x}{\sqrt{1-x^2}\arcsin(x)} \right|
    //\end{eqnarray}
    //</latex>
    //
    // The asin function has a large condition number if
    //   <itemizedlist>
    //   <listitem>x is close to 1,</listitem>
    //   <listitem>x is close to -1.</listitem>
    //   </itemizedlist>
    //
    // Examples
    //  [c, y] = condnb_asincond(0)         // 1
    //  [c, y] = condnb_asincond(0.5)       // c~1
    //  [c, y] = condnb_asincond(1-%eps/2)  // c~10^7
    //  [c, y] = condnb_asincond(-1+%eps/2) // c~10^7
    //  [c, y] = condnb_asincond(1)         // %inf
    //  [c, y] = condnb_asincond(-1)        // %inf
    //
    //  // Vectorized call
    //  condnb_asincond([0 0.1 0.2 0.5])
    //
    //  condnb_plotcond(condnb_asincond, linspace(-1,1,1000));
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_asincond", rhs, 1:1);
    apifun_checklhs("condnb_asincond", lhs, 0:2);

    // Check type
    apifun_checktype("condnb_asincond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    y = asin(x);
    c = zeros(x);
    k = find(x <> 0 ..
           & abs(x) <> 1);
    c(k) =  abs(x(k)) ..
         ./ abs(sqrt(1-x(k).^2) .* y(k));
    k = find(x==0);
    c(k) = 1;
    k = find(abs(x)==1);
    c(k) = %inf;
endfunction

