// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function n=condnb_matrixnorm(varargin)
    // Computes the matrix norm
    //
    // Calling Sequence
    //   n = condnb_matrixnorm(A)
    //   n = condnb_matrixnorm(A, normflag)
    //
    // Parameters
    //   A : a m-by-n matrix of doubles, the matrix
    //   normflag : a double, the norm (default=1). The available values are normflag=1 for the 1-norm, normflag=2 for the Euclidian norm and normflag=%inf for the infinite-norm.
    //   n : a double, the matrix norm
    //
    // Description
    //   Computes the matrix norm of A.
    //
    // The 1-norm of A is :
    //
    //<latex>
    //\begin{eqnarray}
    //\|A\|_1 = \max_{j=1,n} \sum_{i=1}^m |a_{i,j}|
    //\end{eqnarray}
    //</latex>
    //
    // The infinity-norm of A is :
    //
    //<latex>
    //\begin{eqnarray}
    //\|A\|_\infty = \max_{i=1,m} \sum_{j=1}^n |a_{i,j}|
    //\end{eqnarray}
    //</latex>
    //
    // The 2-norm of A is :
    //
    //<latex>
    //\begin{eqnarray}
    //\|A\|_2 = \max_{i=1,r} \sigma_i
    //\end{eqnarray}
    //</latex>
    //
    // where <latex>\sigma_i</latex> are the singular values of 
    // A and r is the rank of A.
    //
    // The goal of this function is to workaround a bug 
    // in Scilab's "norm" function. 
    // Indeed, in the case where A is a 1-by-n row matrix, 
    // Scilab's "norm" uses a vector norm instead of the expected 
    // matrix norm.
    //
    // Examples
    // A=[1 1]
    // condnb_matrixnorm(A)      // 1-norm = 1
    // condnb_matrixnorm(A,1)    // 1-norm = 1
    // condnb_matrixnorm(A,%inf) // INF-norm = 2
    // condnb_matrixnorm(A,2)    // 2-norm = sqrt(2)
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin
    //
    // Bibliography
    //   "Numerical Linear Algebra", Allaire, Kaber, Springer (2007), Chapter 3, Proposition 3.1.2, p.48
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_matrixnorm", rhs, 1:2);
    apifun_checklhs("condnb_matrixnorm", lhs, 0:1);

    // Get arguments
    A = varargin(1);
    normflag = apifun_argindefault(varargin, 2, 1);

    // Check types
    apifun_checktype("condnb_matrixnorm", A, "A", 1, "constant");
    apifun_checktype("condnb_matrixnorm", normflag, "normflag", 2, "constant");

    // Check size
    // A : nothing to check
    apifun_checkscalar("condnb_matrixnorm", normflag, "normflag", 2);

    // Check content
    apifun_checkoption("condnb_matrixnorm", normflag, "normflag", 2, [1 2 %inf]);

    if (normflag==1) then
        n=max(sum(abs(A),"r"))
    elseif (normflag==%inf) then
        n= max(sum(abs(A),"c"))
    elseif (normflag==2) then
        n= max(svd(A))
    else
        n= norm(A,normflag)
    end    
endfunction
