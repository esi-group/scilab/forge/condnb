// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y, jac] = condnb_prodcond(x)
    // Computes the condition number of the product function.
    //
    // Calling Sequence
    //   [c, y, jac] = condnb_prodcond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //   jac : a 1-by-nx matrix of doubles, the Jacobian of f at point x
    //
    // Description
    // Computes the (relative) condition number of the sum function. 
    // This is with respect to the 1-norm for the input (and with 
    // the absolute value norm for the output).
    //
    // The condition number is:
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = \frac{\|x\|_1 \|J(x))\|_1}{|x_1 x_2 ... x_n|}
    //\end{eqnarray}
    //</latex>
    //
    // where the i-th component of the Jacobian matrix J(x) is 
    //
    //<latex>
    //\begin{eqnarray}
    //J_i(x) = \prod_{\substack{j=1,...,n \\ j\neq i}} x_j
    //\end{eqnarray}
    //</latex>
    //
    // The sum function has a large condition number if
    // the sum is small, but the values are of opposite sign
    // and of very different magnitudes.
    //
    // Examples
    //  // A badly conditionned sum, with inaccurate result
    //  xl = 10^(1:15);
    //  x = [-xl xl+0.1];
    //  [c, y] = condnb_prodcond(x);
    //  expected = 1.5;
    //
    //  // A badly conditionned sum, with accurate result !
    //  // The bad conditionning does not imply that the result is not accurate.
    //  xl = 10^(1:15);
    //  x = [-xl xl];
    //  [c, y] = condnb_prodcond(x);
    //  expected = 0;
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_prodcond", rhs, 1:1);
    apifun_checklhs("condnb_prodcond", lhs, 0:3);

    // Check type
    apifun_checktype("condnb_prodcond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    n=size(x,"*")
    y = prod(x);
    if abs(y) == 0 then
        c = %inf;
        jac = zeros(x)'
    else
        // Compute Jacobian matrix
        jac = ones(1,n)*y
        jac = jac./x'
        // Compute 1-norm
        normjac=max(abs(jac))
        c = sum(abs(x)) * normjac / abs(y);
    end
endfunction
