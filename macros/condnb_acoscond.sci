// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y] = condnb_acoscond(x)
    // Computes the condition number of the acos function.
    //
    // Calling Sequence
    //   [c,y] = condnb_acoscond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //
    // Description
    // Computes the condition number of the acos function.
    //
    // The condition number is:
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = \left| \frac{x}{\sqrt{1-x^2}\arccos(x)} \right|
    //\end{eqnarray}
    //</latex>
    //
    // The acos function has a large condition number if
    //   <itemizedlist>
    //   <listitem>x is close to 1,</listitem>
    //   <listitem>x is close to -1,</listitem>
    //   </itemizedlist>
    //
    // Examples
    //  [c, y] = condnb_acoscond(0.5)        // c~1
    //  [c, y] = condnb_acoscond(1-%eps/2)   // c~10^7
    //  [c, y] = condnb_acoscond(-1+%eps/2)  // c~10^7
    //  condnb_acoscond(-1) // %inf
    //  condnb_acoscond(1)  // %inf
    //  condnb_acoscond(0)  // 0
    //
    //  condnb_plotcond(condnb_acoscond , linspace(-1,1,1000) );
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_acoscond", rhs, 1:1);
    apifun_checklhs("condnb_acoscond", lhs, 0:2);

    // Check type
    apifun_checktype("condnb_acoscond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    y = acos(x);
    c = zeros(x);
    k = find(abs(x)<>1);
    c(k) =  abs(x(k)) ..
         ./ abs(sqrt(1-x(k).^2) .* y(k));
    k = find(abs(x)==1);
    c(k) = %inf;
endfunction

