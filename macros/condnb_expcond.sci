// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y] = condnb_expcond(x)
    // Computes the condition number of the exp function.
    //
    // Calling Sequence
    //   [c, y] = condnb_expcond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //
    // Description
    // Computes the condition number of the exp function.
    //
    // The condition number is:
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = |x|
    //\end{eqnarray}
    //</latex>
    //
    // The exp function grows so fast that it is equal to %inf
    // whenever x > 800, and is equal to 0 whenever x < -800.
    // This function has a not so bad condition number, since
    // it is never larger than 1.e2.
    //
    // Examples
    //  [c, y] = condnb_expcond(1);
    //  [c, y] = condnb_expcond(1.e2);
    //
    //  condnb_plotcond(condnb_expcond, linspace(-7e2, 7.e2, 1000));
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_expcond", rhs, 1:1);
    apifun_checklhs("condnb_expcond", lhs, 0:2);

    // Check type
    apifun_checktype("condnb_expcond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    y = exp(x);
    c = abs(x);
endfunction

