// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y] = condnb_atancond(x)
    // Computes the condition number of the atan function.
    //
    // Calling Sequence
    //   [c, y] = condnb_atancond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //
    // Description
    // Computes the condition number of the atan function.
    //
    // The condition number is :
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = \left| \frac{x}{\sqrt{1+x^2}\arctan(x)} \right|
    //\end{eqnarray}
    //</latex>
    //
    // The atan function is well conditionned: it always has a condition number
    // lower than 1.
    //
    // Examples
    //  [c, y] = condnb_atancond(0)        // c=1
    //  [c, y] = condnb_atancond(0.5)      // c~1
    //
    //  condnb_plotcond(condnb_atancond, linspace(-10,10,1000));
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_atancond", rhs, 1:1);
    apifun_checklhs("condnb_atancond", lhs, 0:2);

    // Check type
    apifun_checktype("condnb_atancond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    y = atan(x);
    c = zeros(x);
    k = find(x<>0);
    c(k) =  abs(x(k)) ..
         ./ abs(sqrt(1+x(k).^2) .* y(k));
    k = find(x==0);
    c(k) = 1;
endfunction

