// Copyright (C) 2016 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y, jac] = condnb_condnum(varargin)
    // Computes the empirical condition number of the function f at point x.
    //
    // Calling Sequence
    //   [c, y, jac] = condnb_condnum(f, x)
    //   [c, y, jac] = condnb_condnum(f, x, order)
    //   [c, y, jac] = condnb_condnum(f, x, order, h)
    //   [c, y, jac] = condnb_condnum(f, x, order, h, normflag )
    //
    // Parameters
    //   f : a function with header y = f(x)
    //   x : a nx-by-1 matrix of doubles, the current point
    //   order : a double, integer value, the order (Default order = 2). Available are order = 1, 2, 4. If order == [], then the default order is used.
    //   h : a matrix of doubles, the step. Default tries to be optimal for accuracy. If h == [], then the default h is used.
    //   normflag : a double, the norm (default=1). The available values are normflag=1 for the 1-norm, normflag=2 for the Euclidian norm and normflag=%inf for the infinite-norm.
    //   c : a double, the relative condition number in the given norm 
    //   y : a ny-by-1 matrix of doubles, the computed value of f at point x
    //   jac : a ny-by-nx matrix of doubles, the Jacobian of f at point x
    //
    // Description
    //   Computes the relative condition number by using a finite difference 
    // formula for approximating the Jacobian matrix.
    //
    // The relative condition number is :
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) =  \frac{\|x\| \|J(x)\|}{\|f(x)\|}
    //\end{eqnarray}
    //</latex>
    //
    // This is equation 12.6 in Trefethen, Bau.
    //
    //   Any optional input argument equal to the empty matrix is replaced
    //   by its default value.
    //
    //   The header of the function f must be
    //   <programlisting>
    //     y=f(x)
    //   </programlisting>
    //   where x is the current point and y is the function value.
    //
    // It might happen that the function requires additionnal arguments to
    // be evaluated.
    // In this case, we can use the following feature.
    // The function f can also be the list (fun, a1, a2, ...).
    // In this case fun, the first element in the list, must have the header:
    //   <programlisting>
    //     y = fun(x, a1, a2, ...);
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // will be automatically be appended at the end of the calling sequence.
    //
    // Examples
    //   // Show that sin can be ill-conditionned
    //   [c, y] = condnb_condnum(sin, 1.e-100);         // c~1
    //   // Compare with exact formula:
    //   condnb_sincond(1.e-100);
    //   [c, y] = condnb_condnum(sin, 0);               // c = nan
    //   [c, y] = condnb_condnum(sin, 3.14159);         // c ~10^6
    //   [c, y] = condnb_condnum(sin, 3.141592653);     // c ~10^9
    //   [c, y] = condnb_condnum(sin, 3.1415926535898); // c ~10^14
    //   [c, y] = condnb_condnum(sin, %pi);             // c ~10^16
    //   [c, y] = condnb_condnum(sin, 1.000000357564167061e5); // c ~10^16
    //
    //   // An ill-conditionned case for the sum.
    //   xl = 10^(1:15);
    //   x = [-xl xl+0.1];
    //   yExpected = 1.5;
    //   [c, y] = condnb_condnum(sum, x);
    //   // Compare with exact formula:
    //   cExpected = condnb_sumcond(x); // c~10^15
    //
    //   // Check various finite difference orders for the sqrt function.
    //   // The condition number is 0.5.
    //   cc = condnb_condnum(sqrt, 1, 1);
    //   // Compare with exact formula:
    //   cc = condnb_sqrtcond(1);
    //   cc = condnb_condnum(sqrt, 1, 2);
    //   cc = condnb_condnum(sqrt, 1, 4);
    //   // Make a loop
    //   x = 1.;
    //   order = [1 2 4];
    //   for k = 1:3
    //     cc = condnb_condnum(sqrt, x, order(k));
    //     ce = sqrtcond(x);
    //     disp([order(k) cc ce]);
    //   end
    //
    // // Use default order, but configure h
    // [cc, yc] = condnb_condnum(sqrt, 1, [], 1.e-8);
    // // Use default order and h
    // [cc, yc] = condnb_condnum(sqrt, 1, [], []);
    // // Set order, use default h
    // [cc, yc] = condnb_condnum(sqrt, 1, 4, []);
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    //
    // Bibliography
    //   "Numerical Linear Algebra", Loyd N. Trefethen, David Bau, III, SIAM, Lecture 12, "Conditionning and condition numbers", page 89
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_condnum", rhs, 2:5);
    apifun_checklhs("condnb_condnum", lhs, 0:3);

    // Get arguments
    __condnb_condnum_f = varargin(1);
    x = varargin(2);
    order = apifun_argindefault(varargin, 3, 2);
    h = apifun_argindefault(varargin, 4, []);
    normflag = apifun_argindefault(varargin, 5, 1);
    hflag = (h <> []);

    // Check types
    apifun_checktype("condnb_condnum", __condnb_condnum_f, "f", 1, ..
    ["function" "fptr" "list"]);
    apifun_checktype("condnb_condnum", x, "x", 2, "constant");
    apifun_checktype("condnb_condnum", order, "order", 3, "constant");
    apifun_checktype("condnb_condnum", h, "h", 4, "constant");
    apifun_checktype("condnb_condnum", normflag, "normflag", 5, "constant");

    // Check size
    nx=size(x, "*")
    apifun_checkveccol("condnb_condnum", x, "x", 2, nx);
    apifun_checkscalar("condnb_condnum", order, "order", 3);
    apifun_checkscalar("condnb_condnum", normflag, "normflag", 5);

    // Check content
    apifun_checkoption("condnb_condnum", order, "order", 3, [1 2 4]);
    apifun_checkoption("condnb_condnum", normflag, "normflagrder", 5, [1 2 %inf]);

    // Make x a column vector
    x = x(:);

    y = condnb_condnum_evalf(__condnb_condnum_f, x);

    // Check size of y
    apifun_checkvector("condnb_condnum",y,"y=f(x)",0)
    //
    // Compute jacobian
    jac = numderivative(__condnb_condnum_f, x, h, order);
    normy=condnb_matrixnorm(y,normflag)
    normjac=condnb_matrixnorm(jac,normflag)
    normx=condnb_matrixnorm(x,normflag)
    // Temporary enable IEEE mode, in order to generate 
    // NANs and INFs when appropriate.
    mod=ieee()
    ieee(2)
    c = normx*normjac/normy
    ieee(mod)
endfunction

function y = condnb_condnum_evalf(__condnb_condnum_f, x)
    // Evaluates the function at point x.
    // The function can be a "function" or "fptr" : directly call it.
    // If the function is a list (type=15), the first element is the function,
    // the remaining items are the arguments.
    if type(__condnb_condnum_f) == 15 then
        __condnb_condnum_f_f = __condnb_condnum_f(1);
        y = __condnb_condnum_f_f(x, __condnb_condnum_f(2:$));
    else
        y = __condnb_condnum_f(x);
    end
endfunction

