// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [c, y] = condnb_sqrtcond(x)
    // Computes the condition number of the square root function.
    //
    // Calling Sequence
    //   [c, y] = condnb_sqrtcond(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   c : a n-by-m matrix of doubles, the condition number
    //   y : a n-by-m, matrix of doubles, the function values
    //
    // Description
    // Computes the condition number of the square root function.
    //
    // The condition number is:
    //
    //<latex>
    //\begin{eqnarray}
    //c(x) = 1/2
    //\end{eqnarray}
    //</latex>
    //
    // This function has a good condition number.
    //
    // Examples
    //  [c, y] = condnb_sqrtcond(2);
    //
    //  condnb_plotcond(condnb_sqrtcond, linspace(1, 1.e10, 1000));
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_sqrtcond", rhs, 1:1);
    apifun_checklhs("condnb_sqrtcond", lhs, 0:2);

    // Check type
    apifun_checktype("condnb_sqrtcond", x, "x", 1, "constant");
    // Check size : OK
    // Check content : OK

    y = sqrt(x);
    c = 1/2;
endfunction

