// Copyright(C) 2016 - Michael Baudin
// Copyright(C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function condnb_plotcond(__plotcond_funccbck__, x)
    // Plots the condition number of a function.
    //
    // Calling Sequence
    //   condnb_plotcond(f, x)
    //
    // Parameters
    //   f : a function or a list, the function to plot
    //   x : a m-by-1 or 1-by-m matrix of doubles, the x values.
    //
    // Description
    // Plots the condition number of the function with the function itself.
    // Automatically switches to log-scale for f and its condition, if possible
    // and if necessary.
    // Move the y-label to the left if x data contains mixed signs.
    //
    //   The header of the function f must be
    //   <programlisting>
    //     [c, y] = f(x)
    //   </programlisting>
    //   where x is the current point,
    //   y is the function value and
    //   c is the condition number.
    //
    // It might happen that the function requires additionnal
    // arguments to be evaluated.
    // In this case, we can use the following feature.
    // The function f can also be the list(fun, a1, a2, ...).
    // In this case fun, the first element in the list, must have the header:
    //   <programlisting>
    //     [c, y] = fun(x, a1, a2, ...)
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // will be automatically be appended at the end of the calling sequence.
    //
    // Examples
    //  // The values of atan and its condition are regular:
    //  // use a regular scale.
    //  scf();
    //  condnb_plotcond(condnb_atancond, linspace(-1, 1, 1000));
    //
    //  // The condition values of acos are positive and of
    //  // very different magnitudes:
    //  // automatically switches to log scale.
    //  scf();
    //  condnb_plotcond(condnb_acoscond, linspace(-1, 1, 1000));
    //  scf();
    //  condnb_plotcond(condnb_sincond, linspace(-4, 4, 1000));
    //
    //  // The exp values are positive and of very different magnitudes:
    //  // automatically switches to log scale.
    //  scf();
    //  condnb_plotcond(condnb_expcond, linspace(-7e2, 7.e2, 1000));
    //
    //  // The values of the condition number are very close to zero,
    //  // but we keep the regular scale, as low condition numbers
    //  // are of no interest for us.
    //  scf();
    //  condnb_plotcond(condnb_erfcond, linspace(-6, 6, 1000));
    //
    // // A case where we plot a customized condition number function.
    // function x = myinvnorstd(p)
    //     mu = zeros(p);
    //     std = ones(p);
    //     q = 1 - p;
    //     x = cdfnor("X", mu, std, p, q);
    // endfunction
    // function [c, x] = myinvnorstdcond(p)
    //     for k = 1 : size(p, "*")
    //         [ck, xk] = condnb_condnum(myinvnorstd, p(k));
    //         c(k) = ck;
    //         x(k) = xk;
    //     end
    // endfunction
    // h = scf();
    // condnb_plotcond(myinvnorstdcond, linspace(1e-5, 1-1e-5, 1000));
    //
    // // A case where we plot a customized condition number function,
    // // which needs additionnal arguments.
    // function x = myinvnor(p, mu, std)
    //   q = 1 - p;
    //   x = cdfnor("X", ones(p)*mu, ones(p)*std, p, q);
    // endfunction
    // function [c, x] = myinvnorcond(p, mu, std)
    //   for k = 1 : size(p, "*")
    //     [ck, xk] = condnb_condnum(list(myinvnor, mu, std), p(k));
    //     c(k) = ck;
    //     x(k) = xk;
    //   end
    // endfunction
    // h = scf();
    // condnb_plotcond(list(myinvnorcond, 1, 2), linspace(1e-5, 1-1e-5, 1000));
    //
    // Authors
    // Michael Baudin, DIGITEO, 2010
    //

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_plotcond", rhs, 2:2);
    apifun_checklhs("condnb_plotcond", lhs, 0:1);

    // Check type
    apifun_checktype("condnb_plotcond", __plotcond_funccbck__, "f", 1, ..
    ["function" "fptr" "list"]);
    apifun_checktype("condnb_plotcond", x, "x", 2, "constant");
    // Check size
    apifun_checkvector("condnb_plotcond", x, "x", 2, size(x, "*"));
    // Check content : OK

    // Compute f and its condition number
    // If the function is a list(type=15), the first element is the function,
    // the remaining items are the arguments.
    if type(__plotcond_funccbck__) == 15 then
        __plotcond_funccbck__f_ = __plotcond_funccbck__(1);
        [c, y] = __plotcond_funccbck__f_(x, __plotcond_funccbck__(2:$));
    else
        [c, y] = __plotcond_funccbck__(x);
    end

    h = gcf();
    subplot(2, 1, 1);
    // Plot f
    plot(x, y);
    // Set title
    xtitle("Function Value & Condition Number", "", "f(x)");
    subplot(2, 1, 2);
    // Plot condition
    plot(x, c);
    // Set title
    xtitle("", "x", "Condition Number");
    // Set log scale for the function, if possible and if necessary
    if find(y <= 0) == [] then
        fmax = max(round(log10(y)));
        fmin = min(round(log10(y)));
        // Workaround for bug : http://bugzilla.scilab.org/show_bug.cgi?id=14202
        // Remove "if ( fmax-fmin<600 ) then" when the bug is fixed.
        if ( fmax-fmin<600 ) then
            if fmax > fmin + 2 then
                h.children(2).log_flags = "nln";
            end
        end
    end
    // Set log scale for the condition, if necessary
    c(find(c == 0)) = [];
    cmax = max(round(log10(c)));
    cmin = min(round(log10(c)));
    // Workaround for bug : http://bugzilla.scilab.org/show_bug.cgi?id=14202
    // Remove "if ( fmax-fmin<600 ) then" when the bug is fixed.
    if ( cmax-cmin<600 ) then
        if cmax > cmin + 2 then
            h.children(1).log_flags = "nln";
        end
    end
    // Push the y-label to the left, if necessary
    if min(x) * max(x) < 0 then
        h.children(1).y_location = "left"
        h.children(2).y_location = "left"
    end
endfunction

function argin = argindefault(rhs, vararglist, ivar, default)
    // Returns the value of the input argument #ivar.
    // If this argument was not provided, or was equal to the
    // empty matrix, returns the default value.
    if rhs < ivar then
        argin = default;
    else
        if vararglist(ivar) <> [] then
            argin = vararglist(ivar);
        else
            argin = default;
        end
    end
endfunction

