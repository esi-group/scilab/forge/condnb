// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = condnb_matrixnormvector(varargin)
    // Computes the vector maximizing a given matrix norm
    //
    // Calling Sequence
    //   x = condnb_matrixnormvector(A)
    //   x = condnb_matrixnormvector(A,normflag)
    //
    // Parameters
    //   A : a m-by-n matrix of doubles, the matrix
    //   normflag : a double, the norm (default=1). The available values are normflag=1 for the 1-norm, normflag=2 for the Euclidian norm and normflag=%inf for the infinite-norm.
    //   x : a n-by-p matrix of doubles, the vectors
    //
    // Description
    // Computes the vector x which achieves the matrix given matrix norm. 
    //
    // This vector is so that 
    //
    //<latex>
    //\begin{eqnarray}
    //\frac{\|Ax\|}{\|x\|} = \|A\|
    //\end{eqnarray}
    //</latex>
    //
    // in the sense of the given norm.
    // By definition of the matrix norm, the right-hand side 
    // of the previous equation is the norm of the matrix A. 
    //
    // It might happen that several (say p) vectors x have this property. 
    // In this case, the p columns in x collect all such vectors. 
    //
    // Examples
    // A = [1 1]
    // x = condnb_matrixnormvector(A,1)
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin
    //
    // Bibliography
    //   "Numerical Linear Algebra", Allaire, Kaber, Springer (2007), Chapter 3, Proposition 3.1.2, p.48

    [lhs, rhs] = argn();
    apifun_checkrhs("condnb_matrixnormvector", rhs, 1:2);
    apifun_checklhs("condnb_matrixnormvector", lhs, 0:1);

    // Get arguments
    A = varargin(1);
    normflag = apifun_argindefault(varargin, 2, 1);

    // Check type
    apifun_checktype("condnb_matrixnormvector", A, "A", 1, "constant");
    apifun_checktype("condnb_matrixnormvector", normflag, "normflag", 2, "constant");
    // Check size : OK
    apifun_checkscalar("condnb_matrixnormvector", normflag, "normflag", 2);
    // Check content : OK
    apifun_checkoption("condnb_matrixnormvector", normflag, "normflag", 2, [1 2 %inf]);

    if (normflag==1) then
        x = matrix1normvector(A)
    elseif (normflag==%inf) then
        x = matrixInfnormvector(A)
    elseif (normflag==2) then
        x = matrix2normvector(A)
    else
        error("Not implemented yet.")
    end
endfunction

function x = matrix1normvector(A)
    // Sum the absolute values over the rows : s is a 1-by-n matrix
    asum=sum(abs(A),"r")
    // Find the maximum value
    amax=max(asum)
    // Find all columns j achieving that max
    jmax=find(asum==amax)
    // Create associated vectors x
    n=size(A,"c")
    p=size(jmax,"*")
    x=zeros(n,p)
    for j=1:p
        x(jmax(j),j)=1
    end
endfunction

function x = matrixInfnormvector(A)
    // Sum over the columns : s is a m-by-1 matrix
    asum=sum(abs(A),"c")
    // Find the absolute values the maximum value
    amax=max(asum)
    // Find all rows i achieving that max
    imax=find(asum==amax)
    // Create associated vectors x
    p=size(imax,"*")
    n=size(A,"c")
    x=zeros(n,p)
    for j=1:p
        for k=1:n
            if (A(imax(j),k)==0) then
                x(k, j)=0
            elseif (A(imax(j),k)>0) then
                x(k, j)=1
            elseif (A(imax(j),k)<0) then
                x(k, j)=-1
            end
        end
    end
endfunction

function x = matrix2normvector(A)
    [U,S,V]=svd(A)
    s=diag(S)
    // Find the singular values with the maximum value
    smax=max(s)
    // Find all rows i achieving that max
    imax=find(s==smax)
    // Create associated vectors x
    p=size(imax,"*")
    n=size(A,"c")
    x=zeros(n,p)
    for j=1:p
        x(:, j)=V(:,j)
    end
    //
    // Normalize the vectors, so that the first component 
    // is positive
    for j=1:p
        if (x(1,j)<0) then
            x(:, j)=-x(:, j)
        end
    end
endfunction
