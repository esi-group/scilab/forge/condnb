Condition Number toolbox

Purpose
----

These functions provides the condition number of various elementary functions.
Their goal is to evaluate the potential numerical difficulty of evaluating
numerically a function at a given point.
We may use this module while testing a given function.

The toolbox is based on macros.

To get an overview of the functions in this module, use :

help condnb_overview

This module uses the following modules :
* apifun, v>=0.4


Features
--------

The following is a list of the tutorial help pages :
 * Condition Overview : Condition Overview
 * Tutorial : A tutorial introduction to condition numbers

The following is a list of the current functions :

 * condnb_condnum : Computes the empirical condition number of the function f at point x.
 * condnb_perturbation : Computes the (relative) perturbation
 * condnb_plotcond : Plots the condition number of a function.
 * condnb_worstinput : Computes the input producing the greatest condition number

Scalar Functions
 * condnb_acoscond : Computes the condition number of the acos function.
 * condnb_asincond : Computes the condition number of the asin function.
 * condnb_atancond : Computes the condition number of the atan function.
 * condnb_coscond : Computes the condition number of the cos function.
 * condnb_erfcond : Computes the condition number of the erf function.
 * condnb_erfinvcond : Computes the condition number of the erfinv function.
 * condnb_expcond : Computes the condition number of the exp function.
 * condnb_logcond : Computes the condition number of the log function.
 * condnb_powcond : Computes the condition number of the pow function.
 * condnb_sincond : Computes the condition number of the sin function.
 * condnb_sqrtcond : Computes the condition number of the square root function.
 * condnb_tancond : Computes the condition number of the tan function.

Norms
 * condnb_matrixnorm : Computes the matrix norm
 * condnb_matrixnormvector : Computes the vector maximizing a given matrix norm

Multivariate Functions
 * condnb_prodcond : Computes the condition number of the product function.
 * condnb_sumcond : Computes the condition number of the sum function.

TODO
----

 * ?

Author
----

Copyright (C) 2010 - DIGITEO - Michael Baudin

Licence
----

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

