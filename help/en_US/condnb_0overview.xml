<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * Copyright (C) 2010 - DIGITEO - Michael Baudin
 * 
 -->

<refentry version="5.0-subset Scilab"
          xml:id="condnb_overview"
          xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>Condition Overview</refname>
    <refpurpose>Condition Overview</refpurpose>
  </refnamediv>



  <refsection>
    <title>Description</title>
    <para>
      These functions provides the condition number of various elementary functions.
      Their goal is to evaluate the potential numerical difficulty of evaluating
      numerically a function at a given point.
    </para>

    <para>
      Consider a smooth function f defined by:
    </para>

    <para>
      <latex>
        \begin{eqnarray}
        y = f(x)
        \end{eqnarray}
      </latex>
    </para>

    <para>
      where x and y are a real numbers.
    </para>

    <para>
      The condition number is a measure of the sensitivity of the output y with respect
      to the input x.
      It is the ratio of the relative error on y and the relative error on x.
      For a smooth scalar function f, it is equal to:
    </para>

    <para>
      <latex>
        \begin{eqnarray}
        c_f(x) = \left| \frac{xf'(x)}{f(x)} \right|
        \end{eqnarray}
      </latex>
    </para>

    <para>
      It is possible to express the condition number of the inverse of a function, depending
      on its derivative.
      Indeed, the derivative of the inverse function of f satisfies:
    </para>

    <para>
      <latex>
        \begin{eqnarray}
        \left(f^{-1}\right)^\prime (x) = \frac{1}{f^\prime \left(f^{-1}(x)\right)}
        \end{eqnarray}
      </latex>
    </para>

    <para>
      The condition number of the inverse function is then:
    </para>

    <para>
      <latex>
        \begin{eqnarray}
        c_{f^{-1}} (x) = \left| \frac{x}{f^{-1}(x)f^\prime \left(f^{-1}(x)\right)} \right|
        \end{eqnarray}
      </latex>
    </para>

    <para>
    For several elementary functions, we provide the exact condition number.
    In the case where the condition number is not provided, the user may call the
    <literal>condnb_condnum</literal> function, which uses finite differences to
    estimate it.
    </para>

  </refsection>

  <refsection>
    <title>Quick start</title>

    <para>
    In general, we do not provide the exact condition number of a function. 
    In this case, we may use the <literal>condnb_condnum</literal> function, which uses finite differences to
    estimate it. 
    In the following example, we compute the condition number of the sin function. 
    </para>

   <programlisting role="example"><![CDATA[
[ c , y ] = condnb_condnum ( sin , 1.e-100 )         // c~1
[ c , y ] = condnb_condnum ( sin , 3.14159 )         // c ~10^6
[ c , y ] = condnb_condnum ( sin , 3.141592653 )     // c ~10^9
[ c , y ] = condnb_condnum ( sin , 3.1415926535898 ) // c ~10^14
[ c , y ] = condnb_condnum ( sin , %pi )             // c ~10^16
   ]]></programlisting>

    <para>
    The <literal>condnb_sincond</literal> function provides the exact condition number of the 
    sin function.
    In the following example, we compute the condition number of the sin function
    for various values of x.
    </para>

   <programlisting role="example"><![CDATA[
[ c , y ] = condnb_sincond ( 1.e-100 )         // c~1
[ c , y ] = condnb_sincond ( 3.14159 )         // c ~10^6
[ c , y ] = condnb_sincond ( 3.141592653 )     // c ~10^9
[ c , y ] = condnb_sincond ( 3.1415926535898 ) // c ~10^14
[ c , y ] = condnb_sincond ( %pi )             // c ~10^16
   ]]></programlisting>

    <para>
    The condition number of a function may evolve wildly while the input 
    argument <literal>x</literal> varies in its definition range. 
    In order to analyse the global behavior of the condition number, we may be interested
    by plotting the function value and compare with the condition number in a given interval.
    The condition values of acos are positive and of very different magnitudes :
    automatically switches to log scale.
    </para>

   <programlisting role="example"><![CDATA[
   scf();
   condnb_plotcond ( condnb_acoscond , linspace(-1,1,1000) );
   ]]></programlisting>
   
   <para>
   This produces the following output.
   </para>

   <para>
   <imagedata fileref="../images/acoscond.png" />
   </para>

  </refsection>


  <refsection>
    <title>Condition of Normal Standard Distribution Function</title>

    <para>
    In this section, we analyze the condition number of the normal standard 
    distribution function and its inverse.
    </para>

    <para>
    The following script analyses the condition number of the Inverse normal 
    standard distribution function with respect to p.
    </para>

   <programlisting role="example"><![CDATA[
function x = myinvnorstd ( p )
  mu = zeros(p)
  std = ones(p)
  q = 1-p
  x = cdfnor("X",mu,std,p,q)
endfunction
p = linspace(1e-5,1-1e-5,1000);
for k = 1 : size(p,"*")
  c(k) = condnb_condnum(myinvnorstd,p(k));
end
h = scf();
plot(p,c)
h.children.log_flags="nln";
xtitle("Conditionning of Inverse Normal Standard","p","Condition Number");
   ]]></programlisting>

    <para>
    Notice that, when the function is ill-conditionned for p close to 1,
    then it is well conditionned with respect to q, which is close to 0.
    The only unavoidable ill-conditionned point is p~q~0.5, since both
    p and q make the function ill-conditionned.
    </para>

   <programlisting role="example"><![CDATA[
function x = myinvnorstdq ( q )
  mu = zeros(q)
  std = ones(q)
  p = 1-q
  x = cdfnor("X",mu,std,p,q)
endfunction
q = linspace(1e-5,1-1e-5,1000);
for k = 1 : size(q,"*")
  c(k) = condnb_condnum(myinvnorstdq,q(k));
end
h = scf();
plot(q,c)
h.children.log_flags="nln";
xtitle("Conditionning of Inverse Normal Standard","q","Condition Number");
   ]]></programlisting>

    <para>
    The previous script produces the following plot.
    </para>

   <para>
   <imagedata fileref="../images/invnorstdp.png" />
   </para>

    <para>
    Condition number of normal standard distribution function with respect to p.
   </para>

    <para>
    Notice that the standard normal CDF is equal to zero as soon as x is smaller than -40.
    The condition number is never greater than 1.e4, which is not
    very large, but should be noticed in accuracy tests.
    The symetric behaviour will be observed with respect to q.
    </para>

   <programlisting role="example"><![CDATA[
function p = mynorstd ( x )
  mu = zeros(x)
  std = ones(x)
  p=cdfnor("PQ",x,mu,std)
endfunction
h = scf();
subplot(2,1,1);
x = linspace(-40,10,1000);
p = mynorstd ( x );
plot(x,p);
for k = 1 : size(x,"*")
  c(k) = condnb_condnum(mynorstd,x(k));
end
subplot(2,1,2);
plot(x,c)
xtitle("Conditionning of Normal Standard","x","Condition Number");
h.children(1).y_location = "left";
h.children(2).y_location = "left";
   ]]></programlisting>

    <para>
    The previous script produces the following plot.
    </para>

   <para>
   <imagedata fileref="../images/norstdcond.png" />
   </para>

  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Michael Baudin, DIGITEO, 2010</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>
      "Accuracy and Stability of Numerical Algorithms", Nicholas J. Higham,
      Society for Industrial and Applied Mathematics, Philadelphia, PA, USA, Second Edition,
      2002
    </para>

    <para>"Rounding errors in algebraic processes", James Hardy Wilkinson, 1963, Prentice Hall</para>

    <para>
      "Handbook of Floating-Point Arithmetic", Muller, Brisebarre, de
      Dinechin, Jeannerod, Lefevre, Melquiond, Revol, Stehle, Torres, Birkhauser Boston, 2010
    </para>

  </refsection>

</refentry>
