<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from condnb_condnum.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="condnb_condnum" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>condnb_condnum</refname>
    <refpurpose>Computes the empirical condition number of the function f at point x.</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [c, y, jac] = condnb_condnum(f, x)
   [c, y, jac] = condnb_condnum(f, x, order)
   [c, y, jac] = condnb_condnum(f, x, order, h)
   [c, y, jac] = condnb_condnum(f, x, order, h, normflag )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>f :</term>
      <listitem><para> a function with header y = f(x)</para></listitem></varlistentry>
   <varlistentry><term>x :</term>
      <listitem><para> a nx-by-1 matrix of doubles, the current point</para></listitem></varlistentry>
   <varlistentry><term>order :</term>
      <listitem><para> a double, integer value, the order (Default order = 2). Available are order = 1, 2, 4. If order == [], then the default order is used.</para></listitem></varlistentry>
   <varlistentry><term>h :</term>
      <listitem><para> a matrix of doubles, the step. Default tries to be optimal for accuracy. If h == [], then the default h is used.</para></listitem></varlistentry>
   <varlistentry><term>normflag :</term>
      <listitem><para> a double, the norm (default=1). The available values are normflag=1 for the 1-norm, normflag=2 for the Euclidian norm and normflag=%inf for the infinite-norm.</para></listitem></varlistentry>
   <varlistentry><term>c :</term>
      <listitem><para> a double, the relative condition number in the given norm</para></listitem></varlistentry>
   <varlistentry><term>y :</term>
      <listitem><para> a ny-by-1 matrix of doubles, the computed value of f at point x</para></listitem></varlistentry>
   <varlistentry><term>jac :</term>
      <listitem><para> a ny-by-nx matrix of doubles, the Jacobian of f at point x</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes the relative condition number by using a finite difference
formula for approximating the Jacobian matrix.
   </para>
   <para>
The relative condition number is :
   </para>
   <para>
<latex>
\begin{eqnarray}
c(x) =  \frac{\|x\| \|J(x)\|}{\|f(x)\|}
\end{eqnarray}
</latex>
   </para>
   <para>
This is equation 12.6 in Trefethen, Bau.
   </para>
   <para>
Any optional input argument equal to the empty matrix is replaced
by its default value.
   </para>
   <para>
The header of the function f must be
<programlisting>
y=f(x)
</programlisting>
where x is the current point and y is the function value.
   </para>
   <para>
It might happen that the function requires additionnal arguments to
be evaluated.
In this case, we can use the following feature.
The function f can also be the list (fun, a1, a2, ...).
In this case fun, the first element in the list, must have the header:
<programlisting>
y = fun(x, a1, a2, ...);
</programlisting>
where the input arguments a1, a2, ...
will be automatically be appended at the end of the calling sequence.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Show that sin can be ill-conditionned
[c, y] = condnb_condnum(sin, 1.e-100);         // c~1
// Compare with exact formula:
condnb_sincond(1.e-100);
[c, y] = condnb_condnum(sin, 0);               // c = nan
[c, y] = condnb_condnum(sin, 3.14159);         // c ~10^6
[c, y] = condnb_condnum(sin, 3.141592653);     // c ~10^9
[c, y] = condnb_condnum(sin, 3.1415926535898); // c ~10^14
[c, y] = condnb_condnum(sin, %pi);             // c ~10^16
[c, y] = condnb_condnum(sin, 1.000000357564167061e5); // c ~10^16

// An ill-conditionned case for the sum.
xl = 10^(1:15);
x = [-xl xl+0.1];
yExpected = 1.5;
[c, y] = condnb_condnum(sum, x);
// Compare with exact formula:
cExpected = condnb_sumcond(x); // c~10^15

// Check various finite difference orders for the sqrt function.
// The condition number is 0.5.
cc = condnb_condnum(sqrt, 1, 1);
// Compare with exact formula:
cc = condnb_sqrtcond(1);
cc = condnb_condnum(sqrt, 1, 2);
cc = condnb_condnum(sqrt, 1, 4);
// Make a loop
x = 1.;
order = [1 2 4];
for k = 1:3
cc = condnb_condnum(sqrt, x, order(k));
ce = sqrtcond(x);
disp([order(k) cc ce]);
end

// Use default order, but configure h
[cc, yc] = condnb_condnum(sqrt, 1, [], 1.e-8);
// Use default order and h
[cc, yc] = condnb_condnum(sqrt, 1, [], []);
// Set order, use default h
[cc, yc] = condnb_condnum(sqrt, 1, 4, []);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2016 - Michael Baudin</member>
   <member>Copyright (C) 2010 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Numerical Linear Algebra", Loyd N. Trefethen, David Bau, III, SIAM, Lecture 12, "Conditionning and condition numbers", page 89</para>
</refsection>
</refentry>
